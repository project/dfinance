<?php

namespace Drupal\dfinance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Account Code type entities.
 */
interface AccountCodeTypeInterface extends ConfigEntityInterface {

}
